{{ range $port_map := .PROXY_PORT_MAP | split " " }}
{{ $port_map_list := $port_map | split ":" }}
{{ $scheme := index $port_map_list 0 }}
{{ $listen_port := index $port_map_list 1 }}
{{ $upstream_port := index $port_map_list 2 }}

{{ if eq $listen_port "8080" }}
server {
  listen      [{{ $.NGINX_BIND_ADDRESS_IP6 }}]:{{ $listen_port }} ssl http2;
  listen      {{ if $.NGINX_BIND_ADDRESS_IP4 }}{{ $.NGINX_BIND_ADDRESS_IP4 }}:{{end}}{{ $listen_port }} ssl http2;
  server_name {{ $.APP }}-api.app.softgen.ai;
  access_log  {{ $.NGINX_ACCESS_LOG_PATH }};
  error_log   {{ $.NGINX_ERROR_LOG_PATH }};

  ssl_certificate           {{ $.APP_SSL_PATH }}/server.crt;
  ssl_certificate_key       {{ $.APP_SSL_PATH }}/server.key;
  ssl_protocols             TLSv1.2 TLSv1.3;
  ssl_prefer_server_ciphers off;

  location    / {
    proxy_pass  http://{{ $.APP }}-{{ $upstream_port }};
    proxy_http_version 1.1;
    proxy_read_timeout {{ $.PROXY_READ_TIMEOUT }};
    proxy_buffer_size {{ $.PROXY_BUFFER_SIZE }};
    proxy_buffering {{ $.PROXY_BUFFERING }};
    proxy_buffers {{ $.PROXY_BUFFERS }};
    proxy_busy_buffers_size {{ $.PROXY_BUSY_BUFFERS_SIZE }};
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $http_connection;
    proxy_set_header Host $http_host;
    proxy_set_header X-Forwarded-For {{ $.PROXY_X_FORWARDED_FOR }};
    proxy_set_header X-Forwarded-Port {{ $.PROXY_X_FORWARDED_PORT }};
    proxy_set_header X-Forwarded-Proto {{ $.PROXY_X_FORWARDED_PROTO }};
    proxy_set_header X-Request-Start $msec;
  }

  {{ if $.CLIENT_MAX_BODY_SIZE }}client_max_body_size {{ $.CLIENT_MAX_BODY_SIZE }};{{ end }}
  include {{ $.DOKKU_ROOT }}/{{ $.APP }}/nginx.conf.d/*.conf;
}
{{ else if eq $listen_port "3080" }}
server {
  listen      [{{ $.NGINX_BIND_ADDRESS_IP6 }}]:{{ $listen_port }};
  listen      {{ if $.NGINX_BIND_ADDRESS_IP4 }}{{ $.NGINX_BIND_ADDRESS_IP4 }}:{{end}}{{ $listen_port }};
  server_name {{ $.APP }}.app.softgen.ai;
  access_log  {{ $.NGINX_ACCESS_LOG_PATH }};
  error_log   {{ $.NGINX_ERROR_LOG_PATH }};

  location    / {
    proxy_pass  http://{{ $.APP }}-{{ $upstream_port }};
    proxy_http_version 1.1;
    proxy_read_timeout {{ $.PROXY_READ_TIMEOUT }};
    proxy_buffer_size {{ $.PROXY_BUFFER_SIZE }};
    proxy_buffering {{ $.PROXY_BUFFERING }};
    proxy_buffers {{ $.PROXY_BUFFERS }};
    proxy_busy_buffers_size {{ $.PROXY_BUSY_BUFFERS_SIZE }};
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $http_connection;
    proxy_set_header Host $http_host;
    proxy_set_header X-Forwarded-For {{ $.PROXY_X_FORWARDED_FOR }};
    proxy_set_header X-Forwarded-Port {{ $.PROXY_X_FORWARDED_PORT }};
    proxy_set_header X-Forwarded-Proto {{ $.PROXY_X_FORWARDED_PROTO }};
    proxy_set_header X-Request-Start $msec;
  }

  {{ if $.CLIENT_MAX_BODY_SIZE }}client_max_body_size {{ $.CLIENT_MAX_BODY_SIZE }};{{ end }}
  include {{ $.DOKKU_ROOT }}/{{ $.APP }}/nginx.conf.d/*.conf;
}
{{ end }}
{{ end }}

{{ if $.DOKKU_APP_WEB_LISTENERS }}
{{ range $upstream_port := $.PROXY_UPSTREAM_PORTS | split " " }}
upstream {{ $.APP }}-{{ $upstream_port }} {
{{ range $listeners := $.DOKKU_APP_WEB_LISTENERS | split " " }}
{{ $listener_list := $listeners | split ":" }}
{{ $listener_ip := index $listener_list 0 }}
  server {{ $listener_ip }}:{{ $upstream_port }};{{ end }}
}
{{ end }}{{ end }}
